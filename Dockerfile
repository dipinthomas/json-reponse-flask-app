FROM python:alpine 

LABEL maintainer="dipinthomas2003@gmail.com"

RUN pip install flask

COPY src /src/

EXPOSE 5000

ENTRYPOINT ["python", "/src/app.py"]
